(function ($, Drupal, drupalSettings) {
/**
 * Attaches the imageflow javascript to our images.
 */
Drupal.behaviors.imageflow = {
  attach: function(context, settings) {
    $('[id^="imageflow-"]', context).once('imageflow').each(function() {
      var id = $(this).attr('id');
     var ops = drupalSettings.myimageflow.imageflowJS.settings[id];
    // Eval the opacity array.
      if (ops['opacityArray']) {
        var flowOpacityArray = ops['opacityArray'];
        eval("ops['opacityArray'] = " + flowOpacityArray);
      }
      // Eval the onClick function.
      if (ops['onClick']) {
        var flowClick = ops['onClick'];
        eval("ops['onClick'] = " + flowClick);
      }
      var imageflow = new ImageFlow();
      imageflow.init(ops);
    });
  }
};

})(jQuery, Drupal, drupalSettings);
