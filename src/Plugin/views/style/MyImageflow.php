<?php

namespace Drupal\myimageflow\Plugin\views\style;

use Drupal\core\form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\views\ViewExecutable;
use Drupal\Component\Utility\Html;
use Drupal\views\FieldAPIHandlerTrait;
use Drupal\Core\Field;

/**
 * Style plugin to render fields with imageflow
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "imageflow",
 *   title = @Translation("ImageFlow"),
 *   help = @Translation("Render fields as ImageFlow Carousel"),
 *   theme = "views_view_myimageflow",
 *   display_types = { "normal" }
 * )
 */

class MyImageFlow extends StylePluginBase {
  
  use FieldAPIHandlerTrait;
  /**
   * Does this Style plugin allow fields plugin?
   *
   * @var bool
   */
  protected $usesFields = TRUE;
  
  /**
   * Does this Style plugin allow Row plugins?
   *
   * @var bool
   */
  protected $usesRowPlugin = FALSE;
  
  /**
   * Does this Style plugin uses groupung?
   *
   * @var bool
   */
  protected $usesGrouping = TRUE;
  
  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options += array(
      'imageflow_preset' => array('default' => 'default'),
      'uses_fields'=>TRUE
    );
    return $options;
  }
  
   /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    
    // Is an imagefield available???
    if(!($this->find_image_field())){
      \Drupal::messenger()->addMessage(t('Please chose an image file first.'), 'error'); 
    }
    else{

      $form['imageflow'] = array(
        '#type' => 'fieldset',
        '#title' => 'imageflow',
      );

      // Preset form element.
      $presets = array();
      foreach (myimageflow_presets() as $name => $preset) {
        $presets[$name] = Html::escape($preset['title']);
      }
      if($this->options['imageflow'] && $this->options['imageflow']['imageflow_preset'] ){
        $default_preset = $this->options['imageflow']['imageflow_preset'];
      }
      else{
        $default_preset = $this->options['imageflow_preset'];
      }
      $form['imageflow']['imageflow_preset'] = array(
        '#title' => t('Preset'),
        '#type' => 'select',
        '#options' => $presets,
        '#default_value' => $default_preset,
      );

      // Link options form element.
      $options = array(
        'link_to_file' => 'Link to file',
        'link_to_content' => 'Link to content',
      );
      if (!$this->find_link_field()) {
        \Drupal::messenger()->addMessage($this->t('To change the link individually, use a link field in your content and load it in your view.'), 'alert');
      }
        if($this->find_link_field()){
        $form['imageflow']['imageflow_link'] = array(
          '#title' => t('Link'),
          '#type' => 'select',
          //'#options' => $options,
          '#default_value' => $this->find_link_field(),
          '#access' => FALSE,
        );
      }
      
      $form['image'] = array(
        '#type' => 'select',
        '#title' => "test mich",
        "#default_value" => $this->find_image_field(),
        //"#options" => $this->displayHandler->getFieldLabels(TRUE),
        '#access' => FALSE,
        //'#value' => $this->find_image_field(),
      );
    }
  }
  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state){
    $options = $form_state->getValues('style_options');
    // Pull the fieldset values one level up.
    $options['style_options'] += $options['style_options']['imageflow'];
    unset($options['style_options']['imageflow']);
  }
  
  /**
   * Searches for the image field to use.
   */
  public function find_image_field() {
    $handlers = $this->displayHandler->handlers;
    foreach($handlers['field'] as $id =>$field){
      if($field->options['type'] == "image"){
        //$value = $handlers->getField();
        return $id;
      }
    }
    return FALSE;
  }
  
  /*public function find_link_field() {
  foreach ($this->view->display_handler->getHandlers('field') as $id => $handler) {
    if($handler->getFieldDefinition()->getType() == 'link') { //-> protected properties!!
      ksm("hello");
      return $id;
      break;
    }
    return FALSE;
  }
  }*/
  
  /**
   * Searches for the link field to use, if present.
   */
  public function find_link_field() {
    $handlers = $this->displayHandler->handlers;
    foreach($handlers['field'] as $id =>$field){
      if($field->options['type'] == "link"){
         return $id;
      }
    }
    return FALSE;
  }
  
     /**
    * Render the display in this style.
    */
    /* public function render () {
      $output = '';
      //dpm("hallo?");
        if ($this->usesRowPlugin() && empty($this->view->rowPlugin)) {
    debug('Drupal\\views\\Plugin\\views\\style\\StylePluginBase: Missing row plugin');
    return;
  }
  
  // Group the rows according to the grouping instructions, if specified.
  $sets = $this->renderGrouping($this->view->result, $this->options['grouping'], TRUE);
  return $this->renderGroupingSets($sets);
       // Logs a notice
      //\Drupal::logger('myimageflow')->notice('<pre>'.print_r($this,1)."</pre>");
      /*$image_field = $this->find_image_field();
      if ($image_field === FALSE) {
        drupal_set_message(t('Style @style requires an image field to be added.', array('@style' => $this->definition['title'])), 'error');
        return;
      }
      if ($this->options['imageflow']['imageflow_link'] == 'use_link_field') {
        $link_field = $this->find_link_field();
      }
      else $link_field = '';

      // Group the rows according to the grouping field, if specified.
      $sets = $this->renderGrouping ($this->view->result, $this->options['grouping']);
      // Render each group separately and concatenate.
      foreach ($sets as $title => $rows) {
        $output .= theme($this->theme_functions(),
          array(
            'view' => $this->view,
            'options' => $this->options,
            'img_field_name' => $image_field,
            'link_field_name' => $link_field,
            'rows' => $rows,
            'title' => $title)
          );
      }
      //return $output;
    }*/
}