<?php

namespace Drupal\myimageflow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
/**
 * Implements the ImageFlowPresetAdd form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class MyImageFlowPresetAddForm extends FormBase {

  /**
   * Form for adding a new preset.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   * 
   * @todo
   * make the cancel link correct:
   * https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21Link.php/class/Link/8.6.x
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $form['title'] = array(
      '#type' => 'textfield',
      '#maxlength' => '255',
      '#title' => $this->t('Title'),
      '#description' => $this->t('A human-readable title for this preset.'),
      '#required' => TRUE,
    );
    $form['name'] = array(
      '#type' => 'machine_name',
      '#maxlength' => '255',
      '#machine_name' => array(
        'source' => array('title'),
        'exists' => 'myimageflow_preset_exists',
      ),
      '#required' => TRUE,
    );

    $form['actions'] = array(
      '#type' => 'actions',
      'submit' => array(
        '#type' => 'submit',
        '#value' => $this->t('Create new preset'),
      ),
      'cancel' => array(
        '#type' => 'link',
        '#title' => $this->t('Cancel'),
        '#url' => Url::fromRoute('myimageflow.preset_list'),
      ),
    );

    return $form;
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'myimageflow_preset_add_form';
  }

  /**
   * Submit handler for adding a new preset.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $preset = array(
      'name' => $form_state->getValue('name'),
      'title' => $form_state->getValue('title'),
      'options' => array(
        'buttons' => TRUE,
      ),
    );
    
    $moduleExist = \Drupal::moduleHandler()->moduleExists('imagecache_reflect');
    if ($moduleExist) {
      $preset['imagestyle'] = 'imageflow_reflect';
    }
    $preset = myimageflow_preset_save($preset, TRUE);
    $messenger = \Drupal::messenger();
    $messenger->addMessage($this->t('preset %name was created.', array('%name' => $preset['name'])), $messenger::TYPE_STATUS);
    $dest_url = "/admin/config/media/imageflow/edit/".$preset['name'];
    $url = Url::fromUri('internal:' . $dest_url);
    $form_state->setRedirectUrl($url);
  }
}
