<?php

namespace Drupal\myimageflow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
/**
 * Implements the ImageFlowPresetEdit form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class MyImageFlowPresetEditForm extends FormBase {
  
  /**
   * Array of the settings to handle with.
   *
   * @array
   */
  public $preset;
  
  /**
   * Form for editing a preset.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $preset_name = NULL) {

    if(!isset($this->preset)){
      $this->preset = myimageflow_preset_load($preset_name);
    }

    $form['title'] = array(
      '#type' => 'textfield',
      '#maxlength' => '255',
      '#title' => t('Title'),
      '#default_value' => $this->preset['title'],
      '#description' => t('A human-readable title for this preset.'),
      '#required' => TRUE,
    );

    $form['imagestyle'] = array(
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->preset['imagestyle'],
      '#empty_option' => $this->t('None (original image)'),
      '#options' => image_style_options(FALSE),
    );
    
    if (!(\Drupal::moduleHandler()->moduleExists('imagecache_reflect'))) {
      $form['imagestyle'] += array(
        '#description' => $this->t("Install the image reflect module to add a reflection preset."),
      );
    }
    
    if (\Drupal::moduleHandler()->moduleExists('colorbox')) {
      $form['lightbox'] = array(
        '#title' => $this->t('OnClick behavior'),
        '#type' => 'select',
        '#default_value' => $this->preset['lightbox'],
        '#options' => array(
          'normal' => $this->t('Normal url'),
          'colorbox' => $this->t('Colorbox'),
          'colorboxiframe' => $this->t('Colorbox iframe'),
        ),
      );
    }

    // Option table.
    $form['options'] = array(
      '#type' => 'table',
      '#tree' => TRUE,
      '#header' => array($this->t('Name'), $this->t('Value'), $this->t('Operations')),
      '#prefix' => '<div id="preset-wrapper">',
      '#suffix' => '</div>',
    );
    
    //instead external ajax callback only work in buidForm
    $triggeringElement = $form_state->getTriggeringElement();    
    if($triggeringElement){
      if ($triggeringElement['#name'] == 'add_preset_option'){
        $this->myimageflow_form_preset_edit_submit_add($form, $form_state);
      }
      elseif(strpos($triggeringElement['#name'],'delete_preset_option') !== false){
        $id_del = "";
        // get text after last occurrennce from "_"
        $id_del = substr( strrchr($triggeringElement['#name'], '_' ), 1 );
        $this->myimageflow_form_preset_edit_submit_delete($form, $form_state, $id_del);
      }
    }
    
    //here the rows
    $i = 0;
    foreach ($this->preset['options'] as $key => $value) {
      $option_element = $this->myimageflow_option_element($key, $value);
      if($key){
        $form['options'][] = array(
          'name' => array(
            '#type' => 'item',
            '#title' => Html::escape($key),
            '#description' => isset($option_element['#title']) ? $option_element['#title'] : '',
          ),
          'value' => $option_element + array(
            '#option_name' => $key,
            '#title_display' => 'none',
          ),
          //this is the problem!! no ajax, instead "normal" callback
          'delete' => array(
            '#type' => 'button',
            '#name' => 'delete_preset_option_'.$key,
            '#value' => $this->t('Delete'),
            '#ajax' => array(
                    'callback' =>  '::addmoreCallback',                     
                    'event' => 'click',
                    'wrapper' => 'preset-wrapper',
            ),
          /*  '#attributes' => array(
                    'id' =>  $key
            ),*/
          ),
        );
      }
    }

    // 'Add option' row at the end of the table.
    $options = array_diff(array_keys($this->myimageflow_option_elements()), array_keys($this->preset['options']));
    $options = empty($options) ? array() : array_combine($options, $options);
    $form['options'][] = array(
      'add_option_row' => array(
        '#wrapper_attributes' => array('colspan' => '3', 'class' => array('container-inline')),
        '#tree' => FALSE,
        'new_option' => array(
          '#type' => 'select',
          '#options' => $options,
          '#empty_option' => t('Select or enter:'),
        ),
        'new_option_custom' => array(
          '#type' => 'textfield',
          '#states' => array(
            'visible' => array(
              ':input[name="new_option"]' => array('value' => ''),
            ),
          ),
        ),
        //works as ajax
        'button_add' => array(
          '#type' => 'button',
          '#name' => 'add_preset_option',
          '#value' => $this->t('Add option'),
          '#ajax' => array(
                    'callback' =>  '::addmoreCallback',                    
                    'event' => 'click',
                    'wrapper' => 'preset-wrapper',
            ),
        ),
      ),
    );
    

    $form['actions'] = array(
      '#type' => 'actions',
      'submit' => array(
        '#type' => 'submit',
        '#name' => 'submit',
        '#value' => $this->t('Save preset'),
      ),
      'cancel' => array(
        '#type' => 'link',
        '#title' => $this->t('Cancel'),
        '#url' => Url::fromRoute('myimageflow.preset_list'),
      ),
    );

    return $form;
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'myimageflow_preset_edit_form';
  }
  
  /**
  * Submit handler for 'Add option' button; Add a new option to the set.
  */
  public function myimageflow_form_preset_edit_submit_add(array &$form, FormStateInterface $form_state) {
    
    //updates key/value of presets
    $this->myimageflow_preset_add_key_value($form);
    
    if (!NULL == $form_state->getValue('new_option')) {
      $new_option_element = 'new_option';
    }
    elseif (!NULL == $form_state->getValue('new_option_custom')) {
      $new_option_element = 'new_option_custom';
    }
    if (isset($new_option_element)) {
      $new_option = $form_state->getValue($new_option_element);
      if ($new_option && !array_key_exists($new_option, $this->preset['options'])) {
        // Add the new option with a NULL value.
        $this->preset['options'][$new_option] = NULL;
        drupal_set_message(t('Option %name added.', array('%name' => $new_option)));
      }
      else {
        form_set_error($new_option_element, t('This set already includes the %name option.', array('%name' => $new_option)));
      }
    }
  }
  
  /**
   * Submit handler for 'Delete' buttons; Delete an option from the set.
   */
  public function myimageflow_form_preset_edit_submit_delete(array $form, FormStateInterface &$form_state, $id_del = NULL) {
    
    //updates key/value of presets
    $this->myimageflow_preset_add_key_value($form);    
    if(isset($id_del) && $id_del != ""){
      unset($this->preset['options'][$id_del]);
      drupal_set_message(t('Option %name removed.', array('%name' => $id_del)));
    }
  }
  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the table
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['options'];
  }
  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    // add key|value to $preset
    $this->myimageflow_preset_add_key_value($form);
    
    $this->preset['title'] = $form_state->getValue('title');
    $this->preset['imagestyle'] = $form_state->getValue('imagestyle');
    if (NULL !== ($form_state->getValue('lightbox'))) {
      $this->preset['lightbox'] = $form_state->getValue('lightbox');
    }
    if (isset($this->preset['options'])) {
      $i = 0;
      foreach ($this->preset['options'] as $index => $values) {
        if(isset($index) && $index != "" && isset($form['options'][$i]['value'])){
          $element = $form['options'][$i]['value'];

          if ($values !== '') {
            if ($element['#type'] == 'checkbox') {
              $value = (bool) $values;
            }
            elseif (is_numeric($values)) {
              $value = (float) $values;
            }
            elseif (strcasecmp($values, 'true') == 0) {
              $value = TRUE;
            }
            elseif (strcasecmp($values, 'false') == 0) {
              $value = FALSE;
            }
            else{
              $value = $values;
            }
          }

          $option = $element['#option_name'];
          $this->preset['options'][$option] = $value;
        
          $i++;
        }
      }
    }
    //ksm($this->preset,"Submit preset-was macht sliderCursor?");
    myimageflow_preset_save($this->preset);
    drupal_set_message(t('preset %name changed.', array('%name' => $this->preset['name'])));
    $form_state->setRedirect('myimageflow.preset_list');
  }
  /**
   * Returns the form element to use to edit the given option.
   */
  function myimageflow_option_element($option, $value) {
    $elements = $this->myimageflow_option_elements();
    $element = isset($elements[$option]) ? $elements[$option] : array('#type' => 'textfield');

    if ($value !== NULL) {

       if ($element['#type'] == 'select') {
        if ($value === TRUE) {
          $value = 'true';
        }
        elseif ($value === FALSE) {
          $value = 'false';
        }
      }
      $element['#default_value'] = $value;
    }

    return $element;
  }
  
  /**
   * Option elements.
   * 
   * This function returns an array defining the form elements used to
   * edit the different options.
   */
  public function myimageflow_option_elements() {
    return array(
      'animationSpeed' => array(
        '#type' => 'textfield',
        '#title' => t('Animation speed'),
        '#description' => t('Animation speed in ms.'),
        '#element_validate' => array('::_imageflow_validate_integer'),
        '#default_value' => 50,
      ),
      'aspectRatio' => array(
        '#type' => 'textfield',
        '#title' => t('Aspect ratio'),
        '#description' => t('Aspect ratio of the ImageFlow container (width divided by height).'),
        '#element_validate' => array('::_imageflow_validate_number'),
        '#default_value' => 1.964,
      ),
      'buttons' => array(
        '#type' => 'checkbox',
        '#title' => t('Buttons'),
        '#description' => t('Toggle navigation buttons.'),
        '#default_value' => TRUE,
      ),
      'captions' => array(
        '#type' => 'checkbox',
        '#title' => t('Captions'),
        '#description' => t('Disables / enables the captions.'),
        '#default_value' => TRUE,
      ),
      'circular' => array(
        '#type' => 'checkbox',
        '#title' => t('Circular'),
        '#description' => t('Toggle circular rotation.'),
        '#default_value' => TRUE,
      ),
      'glideToStartID' => array(
        '#type' => 'checkbox',
        '#title' => t('Glide to start ID'),
        '#description' => t('Toggle glide animation to start ID.'),
        '#default_value' => TRUE,
      ),
      'imageCursor' => array(
        '#type' => 'textfield',
        '#title' => t('Image cursor'),
        '#description' => t('Cursor type for the images (try "pointer").'),
        '#default_value' => 'default',
      ),
      'imageFocusM' => array(
        '#type' => 'textfield',
        '#title' => t('imageFocusM'),
        '#description' => t('Multiplicator for the focussed image size in percent.'),
        '#element_validate' => array('::_imageflow_validate_integer'),
        '#default_value' => 1.0,
      ),
      'imageFocusMax' => array(
        '#type' => 'textfield',
        '#title' => t('imageFocusMax'),
        '#description' => t('Maximum number of images on each side of the focussed one.'),
        '#element_validate' => array('::_imageflow_validate_integer'),
        '#default_value' => 4,
      ),
      'imageScaling' => array(
        '#type' => 'checkbox',
        '#title' => t('imageScaling'),
        '#description' => t('Toggle image scaling.'),
        '#default_value' => TRUE,
      ),
      'imagesHeight' => array(
        '#type' => 'textfield',
        '#title' => t('imagesHeight'),
        '#description' => t('Height of the images div container in percent.'),
        '#element_validate' => array('::_imageflow_validate_opacity'),
        '#default_value' => 0.67,
      ),
      'imagesM' => array(
        '#type' => 'textfield',
        '#title' => t('imagesM'),
        '#description' => t('Multiplicator for all images in percent.'),
        '#element_validate' => array('::_imageflow_validate_opacity'),
        '#default_value' => 1.0,
      ),
      'opacity' => array(
        '#type' => 'checkbox',
        '#title' => t('Opacity'),
        '#description' => t('Disables / enables image opacity.'),
        '#default_value' => TRUE,
      ),
      'percentLandscape' => array(
        '#type' => 'textfield',
        '#title' => t('percentLandscape'),
        '#description' => t('Scale landscape format.'),
        '#element_validate' => array('::_imageflow_validate_integer'),
        '#default_value' => 118,
      ),
      'percentOther' => array(
        '#type' => 'textfield',
        '#title' => t('percentOther'),
        '#description' => t('Scale portrait and square format.'),
        '#element_validate' => array('::_imageflow_validate_integer'),
        '#default_value' => 100,
      ),
      'preloadImages' => array(
        '#type' => 'checkbox',
        '#title' => t('preloadImages'),
        '#description' => t('Disables / enables the loading bar and image preloading.'),
        '#default_value' => TRUE,
      ),
      'scrollbarP' => array(
        '#type' => 'textfield',
        '#title' => t('scrollbarP'),
        '#description' => t('Width of the scrollbar in percent.'),
        '#element_validate' => array('::_imageflow_validate_opacity'),
        '#default_value' => 0.6,
      ),
      'slider' => array(
        '#type' => 'checkbox',
        '#title' => t('slider'),
        '#description' => t('Disables / enables the scrollbar.'),
        '#default_value' => TRUE,
      ),
      'sliderCursor' => array(
        '#type' => 'textfield',
        '#title' => t('sliderCursor'),
        '#description' => t('Cursor type for the slider (try "default").'),
        '#default_value' => 'e-resize',
      ),
      'sliderWidth' => array(
        '#type' => 'textfield',
        '#title' => t('sliderWidth'),
        '#description' => t('Width of the slider in px.'),
        '#element_validate' => array('::_imageflow_validate_integer'),
        '#default_value' => 14,
      ),
      'slideshow' => array(
        '#type' => 'checkbox',
        '#title' => t('slideshow'),
        '#description' => t('Toggle slideshow.'),
        '#default_value' => TRUE,
      ),
      'slideshowSpeed' => array(
        '#type' => 'textfield',
        '#title' => t('slideshowSpeed'),
        '#description' => t('Time between slides in ms.'),
        '#element_validate' => array('::_imageflow_validate_integer'),
        '#default_value' => 1500,
      ),
      'slideshowAutoplay' => array(
        '#type' => 'checkbox',
        '#title' => t('slideshowAutoplay'),
        '#description' => t('Toggle automatic slideshow play on startup.'),
        '#default_value' => TRUE,
      ),
      'startID' => array(
        '#type' => 'textfield',
        '#title' => t('startID'),
        '#description' => t('Glide to this image number on startup.'),
        '#element_validate' => array('::_imageflow_validate_integer'),
        '#default_value' => 1,
      ),
      'startAnimation' => array(
        '#type' => 'checkbox',
        '#title' => t('startAnimation'),
        '#description' => t('Animate images moving in from the right on startup.'),
        '#default_value' => TRUE,
      ),
      'xStep' => array(
        '#type' => 'textfield',
        '#title' => t('xStep'),
        '#description' => t('Step width on the x-axis in px.'),
        '#element_validate' => array('::_imageflow_validate_integer'),
        '#default_value' => 150,
      ),
    );
  }
  
  /**
   * updates preset
   * 
   * @param type $form
   */
  public function myimageflow_preset_add_key_value($form){
    $form_options = array_filter ($form['options'], function($key) { return is_int($key); }, ARRAY_FILTER_USE_KEY);
    foreach($form_options as $form_option){
      if(isset($form_option['name'])){
        $form_option_title = $form_option['name']['#title'];
        $form_option_value = $form_option['value']['#value'];
        $this->preset['options'][$form_option_title] = $form_option_value;
      }
    }
  }
  
  /**
 * Validate a form element that should have an integer value.
 */
  public function _imageflow_validate_integer($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value < 0)) {
      $form_state->setErrorByName('validate_integer', t('%name must be a positive integer.', array('%name' => $element['#title'])));
    }
  }
  
  /**
   * Validate a form element that should have a number as value.
   */
  public function _imageflow_validate_number($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if ($value !== '' && !is_numeric($value)) {
      $form_state->setErrorByName('validate_number', t('%name must be a number.', array('%name' => $element['#option_name'])));
    }
  }

  /**
   * Validate a form element that should have a value between 0 and 1.
   */
  public function _imageflow_validate_opacity($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if ($value !== '' && (!is_numeric($value) || $value < 0 || $value > 1)) {
       $form_state->setErrorByName('validate_opacity', t('%name must be a value between 0 and 1.', array('%name' => $element['#option_name'])));
    }
  }
}
