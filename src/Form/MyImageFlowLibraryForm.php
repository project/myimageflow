<?php

namespace Drupal\myimageflow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
/**
 * Implements the ImageFlowPresetEdit form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class MyImageFlowLibraryForm extends FormBase {
  
  /**
   * Array of the settings to handle with.
   *
   * @array
   */
  public $preset;
  
  /**
   * Form for chosing a version of library.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

  $form['library'] = array(
    '#type' => 'fieldset',
    '#title' => 'Imageflow Library',
  );

  $variant =  \Drupal::config('myimageflow.settings')->get('imageflow_library_variant');

  $library = libraries_detect('imageflow');
  $path = $library['library path'];

  if (!$path) {
    \Drupal::messenger()->addMessage($this->t('Imageflow library not found.'), 'error');
    $form['library']['missing'] = array(
      '#type' => 'item',
      '#title' => $this->t('Library missing'),
      '#description' => $this->t('The Imageflow library cannot be found.'),
    );
  }
  else {
    $form['library']['variant'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Library variant'),
      '#default_value' => $variant,
      '#options' => array(
        'source' => $this->t('Source'),
        'minified' => $this->t('Minified'),
      ),
      '#description' => $this->t('Choose whether to use the source or minified version of the imageflow library.'),
      '#required' => TRUE,
    );

    $file = $path . '/' . key($library['variants'][$variant]['files']['js']);
    $form['library']['lib_js_file'] = array(
      '#type' => 'item',
      '#title' => t('Current JS file'),
      '#markup' => $file ? t('<code>@file</code>', array('@file' => $file)) : t('Unknown'),
    );

    $file = $path . '/' . key($library['variants'][$variant]['files']['css']);
    $form['library']['lib_css_file'] = array(
      '#type' => 'item',
      '#title' => t('Current CSS file'),
      '#markup' => $file ? t('<code>@file</code>', array('@file' => $file)) : t('Unknown'),
    );

    $form['library']['info'] = array(
      '#type' => 'table',
      '#tree' => TRUE,
      '#header' => array(t('Name'), t('Value')),
      '#empty' => t('Library not found!'),
    );
    $info = array('name', 'vendor url', 'download url', 'library path');
    foreach ($library as $key => $value) {
      if (in_array($key, $info)) {
        $form['library']['info'][] = array(
          array(
            '#markup' => Html::escape(ucfirst($key)),
          ),
          array(
            '#markup' => t('<code>@file</code>', array('@file' => $value)),
          ),
        );
      }
    }

    $form['library']['button_submit'] = array(
      '#type' => 'submit',
      '#name' => 'button_submit',
      '#value' => $this->t('Submit'),
    );
  }

    return $form;
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'myimageflow_library_form';
  }
  

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('variant');
    $config = \Drupal::service('config.factory')->getEditable('myimageflow.settings');
    $config->set('imageflow_library_variant', $value)->save();
    drupal_flush_all_caches();
    \Drupal::messenger()->addMessage($this->t('Library variant %value saved.', array('%value' => $value)),'alert');
  }
}
