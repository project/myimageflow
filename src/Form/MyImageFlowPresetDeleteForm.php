<?php

namespace Drupal\myimageflow\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
/**
 * Implements the ImageFlowPresetAdd form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class MyImageFlowPresetDeleteForm extends ConfirmFormBase {

  
  /**
   * Name of the item to delete.
   *
   * @string
   */
  protected $preset_name;
  
  /**
   * Form for adding a new preset.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $preset_name = NULL) {
    $this->preset = myimageflow_preset_load($preset_name);
    return parent::buildForm($form, $form_state);
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'myimageflow_preset_delete_form'; 
  }

  /**
   * Submit handler for adding a new preset.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();
    if ($this->preset['name'] == 'default') {
      // Prevent deletion of the default set so we can fall back to it.      
      $messenger->addMessage($this->t('The default preset may not be deleted!'), $messenger::TYPE_ERROR);
    }
    else {
      myimageflow_preset_delete($this->preset);
      $messenger->addMessage($this->t('preset %name was deleted.', array('%name' => $this->preset['name'])), $messenger::TYPE_STATUS);
    }
    $dest_url = "/admin/config/media/imageflow/";
    $url = Url::fromUri('internal:' . $dest_url);
    $form_state->setRedirectUrl($url);
  }
  
  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('myimageflow.preset_list');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Do you want to delete %id?', ['%id' => $this->preset['name']]);
  }
}
