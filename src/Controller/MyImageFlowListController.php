<?php
/**
 * @file
 * Contains \Drupal\myimageflow\Controller\MyImageFlowListController.
 */
namespace Drupal\myimageflow\Controller;
 
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

class MyImageFlowListController extends ControllerBase{
  
  public function table() {
    $presets = myimageflow_presets();

    $header = array(t('preset Name'), array(
        'data' => t('Operations'),
        'colspan' => 2,
      ));
    $rows = array();
    foreach ($presets as $name => $preset) {
      $options = array();
      $rows[] = array(
        Link::fromTextAndUrl(t($preset['title']), 
            Url::fromUri('internal:/admin/config/media/imageflow/edit/' . $name, $options))
          ->toString(),
        Link::fromTextAndUrl(t('edit'), 
            Url::fromUri('internal:/admin/config/media/imageflow/edit/' . $name, $options))
          ->toString(),
      );
      
      end($rows);
      $key = key($rows);
      if($name == 'default'){
        $rows[$key][]= "";
      }
      else{
        $rows[$key][]=
          Link::fromTextAndUrl(t('delete'), 
            Url::fromUri('internal:/admin/config/media/imageflow/delete/' . $name, $options))
          ->toString();
      }
    }
    $build['imageflow_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }
}