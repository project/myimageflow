<?php

/**
 * @file
 * Theme for imageflow views.
 */
function template_preprocess_views_view_myimageflow(&$vars) {
  //\Drupal::logger('myimageflow_theme_inc')->alert('<pre>'.print_r($variables,1).'</pre>'); -> too much, so empty!
  //\Drupal::logger('myimageflow_theme_inc')->alert('hello');
    // View options set by user.
  $view = $vars['view'];
  $style = $view->style_plugin;
  $options = $vars['view']->style_plugin->options;
  $vars['image'] = array();
  
  // Each imageflow instance gets a unique id.
  $imageflow_id = &drupal_static('imageflow_id', 0);
  $vars['id'] = ++$imageflow_id;
  
  // Load the used preset.
  if (!empty($options['imageflow']['imageflow_preset'])) {
    $preset = myimageflow_preset_load($options['imageflow']['imageflow_preset']);
  }
  if (empty($preset)) {
    $preset = myimageflow_preset_load('default');
  }
  
  // Prepare the image field.
  $imageField = $options['image'];
  if (!$imageField || !isset($view->field[$imageField])) {
    template_preprocess_views_view_unformatted($vars);
    return;
  }
  $rows = $vars['rows'];
  $matches_href = $matches_img = $matches_lnk = $matches_alt= array();
  $image = $link = $longdesc = $href =  $img = $lnk = $alt = "";
  foreach ($rows as $rowid =>$row){
    foreach($view->field as $fieldId => $field){
      
      // getFieldValue() is awkward, so this is the alternative
      $field_output = $style->getField($rowid, $fieldId)->__toString();
            
      //get the img without href etc.
      preg_match('/(?<=<img).*\/>/', $field_output, $matches_img);
      if($matches_img){
        $img = $matches_img[0];
        
        // get the href from image
        preg_match('/(?<=a href=\").*(?=\">)/', $field_output, $matches_href);
        if($matches_href){
          $href  = $matches_href[0];
        }
        
        // get the alt string from image
        preg_match('/(?<=alt=\").*?(?=\")/', $field_output, $matches_alt);
        if($matches_alt){
          $alt = $matches_alt[0];
        }
      }
      
      //get the href from link field
      else {
        preg_match('/(?<=a href=\").*(?=\">)/', $field_output, $matches_lnk);
        if($matches_lnk){
          $lnk = $matches_lnk[0];
          $field_output_lnk = $field_output;
        }
      }
      
      if($lnk != ''){
         $link = $lnk;
      }
      elseif ($href != '') {
        $link = $href;
      }
    }
    $longdesc = ($link != '') ? "longdesc =\"".$link."\" " : NULL;
    $image = Drupal\Core\Render\Markup::create("<img " .$longdesc .$img);
    $vars['output_rows'][$rowid]['image'] = $image;
  }
  // Does the admin sets source or mini in his config = settings??
    $variant =  \Drupal::config('myimageflow.settings')->get('imageflow_library_variant');
    if($variant == "source"){
      $vars['#attached']['library'][] = 'myimageflow/myimageflow';
    }
    else{
      $vars['#attached']['library'][] = 'myimageflow/myimageflow.min';
    }
    if($preset['lightbox']== "colorbox" || $preset['lightbox']== "colorboxiframe"){
       $vars['#attached']['library'][] = "colorbox/colorbox";
    }
    
    $vars['#attached']['library'][] = 'myimageflow/myimageflow.init';   
    $settings = myimageflow_add_js($imageflow_id, $preset);
    $vars['#attached']['drupalSettings']['myimageflow']['imageflowJS']['settings'] = $settings;
}
